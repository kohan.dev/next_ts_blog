//GET 'https://simple-blog-api.crew.red/posts'
export const getPosts = [
    {
        "id": 1,
        "title": "Sunt aut facere repellat provident occaecati",
        "body": "Quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "id": 2,
        "title": "Nesciunt iure omnis dolorem tempora et accusantium",
        "body": "Consectetur animi nesciunt iure dolore enim quia ad veniam autem ut quam aut nobis et est aut quod aut provident voluptas autem voluptas"
    },
    {
        "id": 3,
        "title": "Optio molestias id quia eum",
        "body": "Quo et expedita modi cum officia vel magni doloribus qui repudiandae vero nisi sit quos veniam quod sed accusamus veritatis error"
    },
    {
        "id": 4,
        "title": "Sunt aut facere repellat provident occaecati",
        "body": "Quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "id": 5,
        "title": "Nesciunt iure omnis dolorem tempora et accusantium",
        "body": "Consectetur animi nesciunt iure dolore enim quia ad veniam autem ut quam aut nobis et est aut quod aut provident voluptas autem voluptas"
    },
    {
        "id": 6,
        "title": "Optio molestias id quia eum",
        "body": "Quo et expedita modi cum officia vel magni doloribus qui repudiandae vero nisi sit quos veniam quod sed accusamus veritatis error"
    },
    {
        "id": 7,
        "title": "Optio molestias id quia eum",
        "body": "Quo et expedita modi cum officia vel magni doloribus qui repudiandae vero nisi sit quos veniam quod sed accusamus veritatis error"
    },
    {
        "id": 8,
        "title": "Optio molestias id quia eum",
        "body": "Quo et expedita modi cum officia vel magni doloribus qui repudiandae vero nisi sit quos veniam quod sed accusamus veritatis error"
    },
];

//GET 'https://simple-blog-api.crew.red/posts/1?_embed=comments'
export const getPost = {
    "id": 1,
    "title": "Sunt aut facere repellat provident occaecati",
    "body": "Quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
    "comments": [
        {
            "id": 1,
            "postId": 1,
            "body": "Odio adipisci rerum aut animi!"
        },
        {
            "id": 2,
            "postId": 1,
            "body": "dfgdgf dfgdfg Odio adipisci rerum aut animi!"
        },
    ]
};

//POST 'https://simple-blog-api.crew.red/posts' \
// --header 'Content-Type: application/json' \
// --data-raw '{
// 	"title": "In quibusdam tempore odit est dolorem",
// 	"body": "Itaque id aut magnam praesentium quia et odio"
// }'
export const postPost = {
    "title": "In quibusdam tempore odit est dolorem",
    "body": "Itaque id aut magnam praesentium quia et ea odit et ea voluptas et sapiente quia nihil amet occaecati quia id voluptatem incidunt ea est distinctio odio",
    "id": 4
};

//PUT 'https://simple-blog-api.crew.red/posts/1' \
// --header 'Content-Type: application/json' \
// --data-raw '{
// 	"title": "Dolorum ut in voluptas mollitia et saepe quo animi",
// 	"body": "Aut dicta possimus sint mollitia voluptas commodi quo doloremque iste corrupti reiciendis voluptatem eius rerum sit cumque quod eligendi laborum minima perferendis recusandae assumenda consectetur porro architecto ipsum ipsam"
// }'
export const putPost = {
    "title": "Dolorum ut in voluptas mollitia et saepe quo animi",
    "body": "Aut dicta possimus sint mollitia voluptas commodi quo doloremque iste corrupti reiciendis voluptatem eius rerum sit cumque quod eligendi laborum minima perferendis recusandae assumenda consectetur porro architecto ipsum ipsam",
    "id": 1
};

//DELETE 'https://simple-blog-api.crew.red/posts/4'
export const delPost = {};


//POST 'https://simple-blog-api.crew.red/comments' \
// --header 'Content-Type: application/json' \
// --data-raw '{
// 	"postId": 1,
// 	"body": "Maiores sed dolores similique labore et quia et magnam dolor"
// }'
export const postComment = {
    "postId": 1,
    "body": "Maiores sed dolores similique labore et inventore et quasi temporibus esse sunt id et eos voluptatem aliquam aliquid ratione corporis molestiae mollitia quia et magnam dolor",
    "id": 4
};