import {PostBlock, AddPostTitle, AddPostBody, BlogButton} from '../style/styleBlog'


function NewPost(props) {
    return (
        <PostBlock>
            <AddPostTitle
                onChange={props.changeTitle}
            />
            <AddPostBody
                onChange={props.changeBody}
            />
            <BlogButton
                children={'Add Post'}
                onClick={props.handleButton}
            />
        </PostBlock>
    );
}

export default NewPost;