import Link from "next/link";
import {HeaderBlock, Logo} from '../style/styleBlog'


function Header(props) {
    return (
        <HeaderBlock>

            <Link href='/'>
                <Logo>BLOG</Logo>
            </Link>
            <Link href='/posts/new'>
                <Logo>New Post</Logo>
            </Link>
        </HeaderBlock>
    );
}

export default Header;