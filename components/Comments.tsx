import React from "react";
import {HR, PostBlock,BlogButton} from '../style/styleBlog';
import OneComment from "./OneComment";

function Comments(props) {
    const handleButton =() => {

    };

    const comments = () => {
        return props.comments.map((comment) => {
            return (
                <OneComment key={comment.id} comment={comment}/>
            )
        })
    };

    return (
        <PostBlock>
            <HR/>
            {comments()}
            <HR/>
            <BlogButton
                children={'Add Comment'}
                onClick={props.handleButton}
            />
        </PostBlock>
    );
}

export default Comments;