import {ArticleBlock, Title, Body} from '../style/styleBlog'
import Link from "next/link";

function Article(props) {
    const {id, title, body} = props;
    return (
        <ArticleBlock key={id}>
            <Link href={`/posts/id`}>
                <Title>
                    {title}
                </Title>
            </Link>
            <Link href={`/posts/id`}>
                <Body>
                    {body}
                </Body>
            </Link>
        </ArticleBlock>
    );
}

export default Article;