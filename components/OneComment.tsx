import React from "react";
import {CommentBody} from "../style/styleBlog";


function OneComment(props) {
    const {id, body} = props.comment;
    return (
        <CommentBody
            children={`${id}:  ${body}`}
        />
    );
}

export default OneComment;