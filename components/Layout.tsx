import { LayoutBlock, FooterBlock } from "../style/styleBlog";
import Link from "next/link";
import Header from "./Header";


function Layout(props) {
    return (
        <LayoutBlock>
            <Header/>
            {props.children}
            <FooterBlock>
                <p>bla-bla-bla blog</p>
            </FooterBlock>
        </LayoutBlock>
    );
}

export default Layout;