import styled from "styled-components";

export const LayoutBlock = styled.div`
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    min-width: 1060px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    //background-color: #a2fde4;
`;

export const HeaderBlock = styled.div`
    width: 100%;
    height: 80px;
    padding: 18px;
    margin-bottom: 20px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    text-decoration: none;
    justify-content: space-between;
    color: #f0f8ff;
    background-color: #191863;
`;

export const Logo = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
`;

export const ArticlesBlock = styled.div`
    margin: 0;
    padding: 1px;
    max-width: 1120px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    //background-color: burlywood;
`;

export const ArticleBlock = styled.div`
    margin: 8px 5px;
    padding: 15px;
    width: 300px;
    height: 200px;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: flex-start;
    cursor: pointer;
    box-shadow: 0 2px 3px rgba(25,24,99,0.33);
    border-radius: 5px;
`;

export const Title = styled.p`
    padding-bottom: 15px;
    text-align: center;
    cursor: pointer;
    font-weight: bold;
    font-style: italic;
    color: #191863;
`;

export const Body = styled.p`
    padding-bottom: 10px;
    text-align: left;
    color: rgba(18,18,45,0.84);
`;

export const PostBlock = styled.div`
    margin: 0;
    padding: 20px;
    width: 80%;
    min-width: 300px;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`;

export const PostTitle = styled.h1`
    margin-bottom: 30px;
    width: 80%;
    text-align: center;
    color: #191863;
    font-size: large;
    font-weight: bold;
`;

export const PostBody = styled.p`
    width: 80%;
    padding: 15px;
    color: rgba(18,18,45,0.84);
`;

export const AddPostTitle = styled.input`
    padding: 4px;
    margin-bottom: 20px;
    height: 24px;
    width: 80%;
    color: #191863;
    font-weight: bold;
    border: 1px solid #191863;
    border-radius: 4px;
`;

export const AddPostBody = styled.textarea`
    height: 250px;
    width: 80%;
    padding: 4px;
    margin-bottom: 30px;
    resize: none;
    border: 1px solid #191863;
    border-radius: 4px;
`;

export const BlogButton = styled.button`
    height: 36px;
    width: 200px;
    border: 0 solid;
    border-radius: 4px;
    background-color: #191863;
    color: #f0f8ff;
`;

export const CommentBody = styled.div`
    height: 60px;
    width: 80%;
    padding: 15px;
    margin-bottom: 4px;
    color: #5c6574;
    border: 1px solid #5c6574;
    border-radius: 4px;
`;


export const FooterBlock = styled.div`
    width: 100%;
    height: 120px;
    padding: 0;
    margin-top: 120px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #f0f8ff;
    background-color: #191863;
`;

export const HR = styled.hr`
    height: 1px;
    width: 500px;
    margin: 15px 0;
    border: 1px solid rgba(18,18,45,0.84);
`;



