import Layout from "../../components/Layout";
import {PostBlock, PostTitle, PostBody} from "../../style/styleBlog";
import {getPost} from "../../src/mock/mock";
import Comments from "../../components/Comments";

function Id(props) {
    const post = () => {
        return (
            <>
                <PostTitle
                    children={getPost.title}
                />
                <PostBody
                    children={getPost.body}
                />

                <Comments
                    comments={getPost.comments}
                />
            </>
        )
    };

    return(
        <Layout>
            <PostBlock>
                {post()}
            </PostBlock>

        </Layout>

);
}

export default Id;