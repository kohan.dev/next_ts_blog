import Layout from "../../components/Layout";
import NewPost from "../../components/NewPost";

function New(props) {

    const handleButton = () => {

    };

    const changeBody = () => {

    };

    const changeTitle =() => {

    };

    return (
        <Layout>
            <p>NEW POST</p>
            <NewPost
                changeTitle={changeTitle}
                changeBody={changeBody}
                handleButton={handleButton}
            />
        </Layout>
    );
}

export default New;