import Head from "next/head";
import Layout from "../components/Layout";
import Article from "../components/Article";
import {ArticlesBlock} from "../style/styleBlog";
import {getPosts} from "../src/mock/mock";

export default function Home() {
    const articles = () => {
        return (
            getPosts.map((post) => {
                return (
                    <Article
                        id={post.id}
                        title={post.title}
                        body={post.body}
                    />
                )

            })
        )
    };

    return (
        <Layout>
            <Head>
                <title>TEST BLOG</title>
                <link rel="icon" href="/favicon.ico" />
                <meta charSet={'UTF-8'}/>
                <meta name="description" content="Good blog abot all" />
                <meta name="keywords" content="blog, test, next.js, react.js, node.js" />
                <meta property="og:url" content="https://bla-bla-bla-blog.org/" />
                <meta property="og:title" content="The best blog test Next.js" />
            </Head>
            <ArticlesBlock>
                {articles()}
            </ArticlesBlock>

        </Layout>
    )
}
